﻿using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using ApiApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ApiApp.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        async private Task<string> MakePost(string request)
        {
            using (var client = new HttpClient())
            {
                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, "http://localhost:5000/api/search");
                requestMessage.Content = new StringContent(request, Encoding.UTF8, "application/json");

                var response = await client.SendAsync(requestMessage);

                string textResult = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    return $"Result = {JsonConvert.DeserializeObject<string>(textResult)}";
                else
                {
                    return $"Return code = {response.StatusCode}, result = {textResult}";
                }
            }
        }

        [HttpGet]
        public string Get()
        {
            return "Use GET to /api/search/success or /api/search/failure and the system will make a POST to /api/search to get results.\nUse GET to /api/search/multiple?filterparam=\"X\"&filtervalue=\"Y\" to test query string with multiple parameters with the same name.";
        }

        [HttpGet("multiple")]
        public string GetMultiple([FromQuery] string[] filterparam, [FromQuery] string[] filtervalue)
        {
            string result = "";
            
            for (int i = 0; i < filterparam.Length; i++)
            {
                if (i >= filtervalue.Length)
                    break;
                result += $"\n   {filterparam[i]} : {filtervalue[i]}";
            }
            return $"Query IDs are: {result}";
        }

        [HttpGet("success")]
        public Task<string> PostSuccess()
        {
            var requestJSON = new PostRequest()
            {
                ISBN = "2-266-11156-6"
            };

            return MakePost(JsonConvert.SerializeObject(requestJSON));
        }

        [HttpGet("failure")]
        public Task<string> PostFail()
        {

            var requestJSON = new PostRequest()
            {
                ISBN = "2-266-11156-7"
            };

            return MakePost(JsonConvert.SerializeObject(requestJSON));
        }

        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces(MediaTypeNames.Application.Json)]
        public ActionResult<string> Post(PostRequest value)
        {
            if (value.ISBN != "2-266-11156-6")
                return NotFound();

            var responceJSON = new PostResponce()
            {
                libraryName = "Название библиотеки",
                bookName = "Название книги",
                bookUrl = "http://localhost:5000/books/100"
            };

            return JsonConvert.SerializeObject(responceJSON);
        }
    }
}