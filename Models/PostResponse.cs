﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiApp.Models
{
    public class PostResponce
    {
        [JsonProperty("library_name")]
        public string libraryName;

        [JsonProperty("book_name")]
        public string bookName;

        [JsonProperty("url")]
        public string bookUrl;
    }
}
